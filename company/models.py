from django.core.validators import RegexValidator
from django.db import models
from django.urls import reverse
from django.template.defaultfilters import slugify as django_slugify
from company.const import alphabet


def slugify(s):
    """
    Overriding django slugify that allows to use russian words as well.
    """
    return django_slugify(''.join(alphabet.get(w, w) for w in s.lower()))


class Company(models.Model):                                    #Компании
    name = models.CharField( max_length=50, unique=True)        #Название
    slug = models.SlugField(max_length=1000)                    #url
    leader = models.CharField(max_length=50)                    #ФИО контактного лица
    info = models.CharField(max_length=250)                     #Описание
    address = models.CharField(max_length=250)                  #Адресс
    is_active = models.BooleanField(default=True)               #Признак активности
    created_at = models.DateTimeField(auto_now_add=True)        #Создание записи
    updated_at = models.DateTimeField(auto_now=True)            #Обновление записи

    class Meta:
        ordering = ('name',)
        verbose_name = 'company'
        verbose_name_plural = 'companies'

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('com_detail', args=[self.slug])

    def __str__(self):
        return self.name


class Project(models.Model):                                        #Проэкты
    name = models.CharField(max_length=250)                         #Название
    slug = models.SlugField(max_length=250, unique=True)            #url
    description = models.CharField(max_length=250)                  #Описание
    date_begin = models.DateField()                             #Начало проекта
    date_end = models.DateField()                               #Конец проекта
    is_active = models.BooleanField(default=True)                   #Признак активности
    price = models.DecimalField(max_digits=10, decimal_places=2)    #Стоимость проекта
    created_at = models.DateTimeField(auto_now_add=True)            #Создание записи
    updated_at = models.DateTimeField(auto_now=True)                #Обновление записи
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='companies_projects')  #Компания

    class Meta:
        ordering = ('name',)
        verbose_name = 'project'
        verbose_name_plural = 'projects'

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('pro_detail', args=[self.slug])

    def __str__(self):
        return self.name


class Email(models.Model):                                                      #Почта
    email_regex = RegexValidator(regex=r"^b[A-Za-z0-9.%+-]+@[A-Z|a-z]{2,}\b$",
                                 message='Введите корректный E-mail адрес(например, google@mail.com)')
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='companies_emails')  #Компания
    email = models.CharField(validators=[email_regex], max_length=50, unique=False, verbose_name='Email') #Почтовый адрес
    created_at = models.DateTimeField(auto_now_add=True)            #Создание записи
    updated_at = models.DateTimeField(auto_now=True)                #Обновление записи

    class Meta:
        ordering = ('company',)
        verbose_name = 'email'
        verbose_name_plural = 'emails'

    def __str__(self):
        return self.email


class Phone(models.Model):                                          #Телефоны
    phone_regex = RegexValidator(regex=r"^\+380\d{3}\d{2}\d{2}\d{2}$",
                                 message='Введите корретный номер телефона(например, +3801234567890)')
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='companies_phones')  #Компания
    phone = models.CharField(validators=[phone_regex], max_length=15, unique=True, verbose_name='Номер телефона') #Номер телефона
    created_at = models.DateTimeField(auto_now_add=True)            #Создание записи
    updated_at = models.DateTimeField(auto_now=True)                #Обновление записи

    class Meta:
        ordering = ('company',)
        verbose_name = 'phone'
        verbose_name_plural = 'phones'

    def __str__(self):
        return self.phone


class Massage(models.Model):                                                #Сообщения
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name='projects_massage')  #Проект
    tagline = models.CharField(max_length=100, default='')          #Теги
    text = models.TextField(max_length=5000)                         #Текст сообщения
    created_at = models.DateTimeField(auto_now_add=True)            #Создание записи
    updated_at = models.DateTimeField(auto_now=True)                #Обновление записи

    class Meta:
        ordering = ('project',)
        verbose_name = 'massage'
        verbose_name_plural = 'massages'

    def save(self, *args, **kwargs):
        self.slug = self.pk
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('pro_detail', args=[self.slug])

    def __str__(self):
        return self.slug


class Users(models.Model):                                          #Пользователи
    username = models.CharField(max_length=12, unique=True)         #Имя пользователя
    password = models.CharField(max_length=18)                      #Пароль
    first_name = models.CharField(max_length=12)                    #Имя
    last_name = models.CharField(max_length=12)                     #Фамилия
    photo = models.ImageField()                                     #Фото
    phone = models.CharField(max_length=250)                        #Телефон
    email = models.CharField(max_length=250)                        #Почта
    massage = models.ForeignKey(Massage, on_delete=models.CASCADE, related_name='massages_user')  #Сообщение
    is_active = models.BooleanField(default=True)                   #Активность
    is_stuff = models.BooleanField(default=False)                   #Пренадлежность к администрации
    created_at = models.DateTimeField(auto_now_add=True)            #Создание записи
    updated_at = models.DateTimeField(auto_now=True)                #Обновление записи

    def __str__(self):
        return self.username


class RatingStars(models.Model):                                    #Рейтинги(звезд)
    value = models.PositiveSmallIntegerField(default=0)             #Значение звезд

    def __str__(self):
        return self.value

    class Meta:
        verbose_name = "Raiting Star"
        verbose_name_plural = "Raiting Stars"


class Raiting(models.Model):                                        #Рейтинги
    ip = models.CharField(max_length=15)
    star = models.ForeignKey(RatingStars, on_delete=models.CASCADE, verbose_name="star")
    massage = models.ForeignKey(Massage, on_delete=models.CASCADE, verbose_name="massage")

    def __str__(self):
        return f"{self.star} - {self.massage}"

    class Meta:
        verbose_name = "Rating"
        verbose_name_plural = "Ratings"
